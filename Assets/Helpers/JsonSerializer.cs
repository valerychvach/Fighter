﻿using UnityEngine;
using System.Collections;
using System;

public class TinyJsonSerializer : ISerializer
{
    public T Deserialize<T>(string obj)
    {
        return TinyJSON.Decoder.Decode(obj).Make<T>();
        //return JsonUtility.FromJson<T>(obj);
    }

    public string Serialize(object obj)
    {
        //   return JsonUtility.ToJson(obj);
        return TinyJSON.Encoder.Encode(obj);
    }
}

public class JsonSerializer : ISerializer
{
    public T Deserialize<T>(string obj)
    {
        return JsonUtility.FromJson<T>(obj);
    }

    public string Serialize(object obj)
    {
        return JsonUtility.ToJson(obj);
        // return TinyJSON.Encoder.Encode(obj);
    }
}