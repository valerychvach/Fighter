﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters;

public class BinarySerializer : ISerializer
{
    private BinaryFormatter _bf;

    public BinarySerializer()
    {
        _bf = new BinaryFormatter();
        _bf.AssemblyFormat = FormatterAssemblyStyle.Full;
    }

    public string Serialize(object obj)
    {
        MemoryStream memoryStream = new MemoryStream();
        _bf.Serialize(memoryStream, obj);
        return Convert.ToBase64String(memoryStream.ToArray());
    }

    public T Deserialize<T>(string obj)
    {
        MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(obj));
        return (T) _bf.Deserialize(memoryStream);
    }
}