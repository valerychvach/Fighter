﻿using System;
using UnityEngine;
using System.Collections;

public class SerializeFactory<TISerializer> where TISerializer : ISerializer, new()
{
    public static string Serialize<T>(T obj)
    {
        var serializer = CreateSerializer();
        return serializer.Serialize(obj);
    }

    public static T Deserialize<T>(string obj)
    {
        var serializer = CreateSerializer();
        return serializer.Deserialize<T>(obj);
    }

    private static TISerializer CreateSerializer()
    {
        return new TISerializer();
    }
}