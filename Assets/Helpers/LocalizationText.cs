﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizationText : MonoBehaviour
{
    [SerializeField] private string _key;
    [SerializeField] private string _defaultValue;

    //private ILocalizationManager _localizationManager;
    private Text _text;

    private void Start()
    {
        //_localizationManager = ManagerLocator.Get<ILocalizationManager>();
        _text = GetComponent<Text>();
        SetText();
        //_localizationManager.OnLanguageChanged += OnLanguageChanged;
    }

    public void SetText()
    {
        //_text.text = _localizationManager.GetText(_key, _defaultValue);

        if (Regex.IsMatch(_text.text, @"\p{IsCyrillic}"))
        {
            _text.fontStyle = FontStyle.Bold;
        }
        else
        {
            _text.fontStyle = FontStyle.Normal;
        }
    }

    public void OnLanguageChanged()
    {
        SetText();
    }

    //private void OnDestroy()
    //{
    //    if (_localizationManager != null)
    //        _localizationManager.OnLanguageChanged -= OnLanguageChanged;
    //}
}