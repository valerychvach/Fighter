﻿using UnityEngine;
using System.Collections;

public interface ISerializer
{
    string Serialize(object obj);
    T Deserialize<T>(string obj);
}