﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotkeyManager : MonoBehaviour
{
    //private string keyKode

    private void Update()
    {
        GetButtonsDown();
    }
    private void GetButtonsDown()
    {
        if (Input.GetKeyDown("q"))
        {
            SkillActionController.Instance().Button1();
        }
        else if (Input.GetKeyDown("w"))
        {
            SkillActionController.Instance().Button2();
        }
        else if (Input.GetKeyDown("e"))
        {
            SkillActionController.Instance().Button3();
        }
        else if (Input.GetKeyDown("r"))
        {
            SkillActionController.Instance().Button4();
        }
        else if (Input.GetKeyDown("t"))
        {
            SkillActionController.Instance().Button5();
        }
        else if (Input.GetKeyDown("a"))
        {
            SkillActionController.Instance().Armor();
        }
        else if (Input.GetKeyDown("h"))
        {
            SkillActionController.Instance().Heal();
        }
    }
}
