﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogElement
{
    public string SkillName;
    public float Enrage;
    public float Super;
    public float Health;
    public float Armor;
}
