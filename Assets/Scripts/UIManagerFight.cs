﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManagerFight : MonoBehaviour
{
    [Header("Button1")]
    [SerializeField] private Button button1;
    [SerializeField] private Transform panelCooldownButton1;
    [SerializeField] private TextMeshProUGUI textCooldownButton1;
    [Header("Button2")]
    [SerializeField] private Button button2;
    [SerializeField] private Transform panelCooldownButton2;
    [SerializeField] private TextMeshProUGUI textCooldownButton2;
    [Header("Button3")]
    [SerializeField] private Button button3;
    [SerializeField] private Transform panelCooldownButton3;
    [SerializeField] private TextMeshProUGUI textCooldownButton3;
    [Header("Button4")]
    [SerializeField] private Button button4;
    [SerializeField] private Transform panelCooldownButton4;
    [SerializeField] private TextMeshProUGUI textCooldownButton4;
    [Header("Button5")]
    [SerializeField] private Button button5;
    [SerializeField] private Transform panelCooldownButton5;
    [SerializeField] private TextMeshProUGUI textCooldownButton5;
    [Header("Armor")]
    [SerializeField] private Button btnArmor;
    [SerializeField] private Transform panelCooldownArmor;
    [SerializeField] private TextMeshProUGUI textCooldownArmor;
    [Header("Heal")]
    [SerializeField] private Button btnHeal;
    [SerializeField] private Transform panelCooldownHeal;
    [SerializeField] private TextMeshProUGUI textCooldownHeal;
    // Ready to use skills
    [SerializeField] public bool readyButton1;
    [SerializeField] public bool readyButton2;
    [SerializeField] public bool readyButton3;
    [SerializeField] public bool readyButton4;
    [SerializeField] public bool readyButton5;
    [SerializeField] public bool readyArmor;
    [SerializeField] public bool readyHeal;
    // Skills cooldown
    private float timeCooldownButton1;
    private float timeCooldownButton2;
    private float timeCooldownButton3;
    private float timeCooldownButton4;
    private float timeCooldownButton5;
    private float timeCooldownArmor;
    private float timeCooldownHeal;
    // Timer for chacking
    private float timeForChecking = 0;

    // Instance
    private static UIManagerFight instance;
    public static UIManagerFight Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        EnableAllButton();
    }
    private void FixedUpdate()
    {
        timeForChecking += Time.fixedDeltaTime;
        if (timeForChecking >= 0.2f)
        {
            if (!VideoManager.Instance().isPlay)
            {
                CheckActiveSkills();
                timeForChecking = 0f;
            }
            else
            {
                DisInteractableAllButtons();
            }
            
        }
        
        Button1Timer();
        Button2Timer();
        Button3Timer();
        Button4Timer();
        Button5Timer();
        ArmorTimer();
        HealTimer();
    }

    private void CheckActiveSkills()
    {
        // Check attack1
        if (UIManagerBars.Instance().currentSuper >= FightManager.Instance().skillList[0].Ready)
        {
            button1.interactable = true;
            
        }
        else
        {
            button1.interactable = false;
        }
        // Check attack2
        if (UIManagerBars.Instance().currentSuper >= FightManager.Instance().skillList[1].Ready)
        {
            button2.interactable = true;
        }
        else
        {
            button2.interactable = false;
        }
        // Check attack3
        if (UIManagerBars.Instance().currentSuper >= FightManager.Instance().skillList[2].Ready)
        {
            button3.interactable = true;
        }
        else
        {
            button3.interactable = false;
        }
        // Check button4
        if (UIManagerBars.Instance().currentSuper >= FightManager.Instance().skillList[3].Ready)
        {
            button4.interactable = true;
        }
        else
        {
            button4.interactable = false;
        }
        // Check Combo1
        if (UIManagerBars.Instance().currentSuper >= FightManager.Instance().skillList[4].Ready)
        {
            button5.interactable = true;
        }
        else
        {
            button5.interactable = false;
        }
        // Check Armor
        if (UIManagerBars.Instance().currentSuper >= FightManager.Instance().skillList[5].Ready)
        {
            btnArmor.interactable = true;
        }
        else
        {
            btnArmor.interactable = false;
        }
        // Check Heal
        if (UIManagerBars.Instance().currentSuper >= FightManager.Instance().skillList[6].Ready)
        {
            btnHeal.interactable = true;
        }
        else
        {
            btnHeal.interactable = false;
        }
    }

    // Skill actions
    public void Button1Cooldown(float timeCooldown)
    {
        if (!readyButton1)
            return;

        timeCooldownButton1 = timeCooldown;
        panelCooldownButton1.gameObject.SetActive(true);
        button1.interactable = false;
        readyButton1 = false;
    }
    public void Button2Cooldown(float timeCooldown)
    {
        if (!readyButton2)
            return;

        timeCooldownButton2 = timeCooldown;
        panelCooldownButton2.gameObject.SetActive(true);
        button2.interactable = false;
        readyButton2 = false;

        // Chance restart Attack3 cooldown
        if (!readyButton3 && Random.Range(0, 100) <= 50)
        {
            timeCooldownButton3 = -1f;
        }

    }
    public void Button3Cooldown(float timeCooldown)
    {
        if (!readyButton3)
            return;

        timeCooldownButton3 = timeCooldown;
        panelCooldownButton3.gameObject.SetActive(true);
        button3.interactable = false;
        readyButton3 = false;
    }
    public void Button4Cooldown(float timeCooldown)
    {
        if (!readyButton4)
            return;

        timeCooldownButton4 = timeCooldown;
        panelCooldownButton4.gameObject.SetActive(true);
        button4.interactable = false;
        readyButton4 = false;
    }
    public void Button5Cooldown(float timeCooldown)
    {
        if (!readyButton5)
            return;

        timeCooldownButton5 = timeCooldown;
        panelCooldownButton5.gameObject.SetActive(true);
        button5.interactable = false;
        readyButton5 = false;
    }
    public void ArmorCooldown(float timeCooldown)
    {
        if (!readyArmor)
            return;

        timeCooldownArmor = timeCooldown;
        panelCooldownArmor.gameObject.SetActive(true);
        btnArmor.interactable = false;
        readyArmor = false;
    }
    public void HealCooldown(float timeCooldown)
    {
        if (!readyHeal)
            return;

        timeCooldownHeal = timeCooldown;
        panelCooldownHeal.gameObject.SetActive(true);
        btnHeal.interactable = false;
        readyHeal = false;
    }

    // Skills timer
    private void Button1Timer()
    {
        if (readyButton1)
            return;

        textCooldownButton1.text = (timeCooldownButton1 + 0.99f).ToString().Split('.')[0];
        timeCooldownButton1 -= Time.fixedDeltaTime;

        if (timeCooldownButton1 <= 0)
        {
            panelCooldownButton1.gameObject.SetActive(false);
            button1.interactable = true;
            readyButton1 = true;
        }
    }
    private void Button2Timer()
    {
        if (readyButton2)
            return;

        textCooldownButton2.text = (timeCooldownButton2 + 0.99f).ToString().Split('.')[0];
        timeCooldownButton2 -= Time.fixedDeltaTime;

        if (timeCooldownButton2 <= 0)
        {
            panelCooldownButton2.gameObject.SetActive(false);
            button2.interactable = true;
            readyButton2 = true;
        }
    }
    private void Button3Timer()
    {
        if (readyButton3)
            return;

        textCooldownButton3.text = (timeCooldownButton3 + 0.99f).ToString().Split('.')[0];
        timeCooldownButton3 -= Time.fixedDeltaTime;

        if (timeCooldownButton3 <= 0)
        {
            panelCooldownButton3.gameObject.SetActive(false);
            button3.interactable = true;
            readyButton3 = true;
        }
    }
    private void Button4Timer()
    {
        if (readyButton4)
            return;

        textCooldownButton4.text = (timeCooldownButton4 + 0.99f).ToString().Split('.')[0];
        timeCooldownButton4 -= Time.fixedDeltaTime;

        if (timeCooldownButton4 <= 0)
        {
            panelCooldownButton4.gameObject.SetActive(false);
            button4.interactable = true;
            readyButton4 = true;
        }
    }
    private void Button5Timer()
    {
        if (readyButton5)
            return;

        textCooldownButton5.text = (timeCooldownButton5 + 0.99f).ToString().Split('.')[0];
        timeCooldownButton5 -= Time.fixedDeltaTime;

        if (timeCooldownButton5 <= 0)
        {
            panelCooldownButton5.gameObject.SetActive(false);
            button5.interactable = true;
            readyButton5 = true;
        }
    }
    private void ArmorTimer()
    {
        if (readyArmor)
            return;

        textCooldownArmor.text = (timeCooldownArmor + 0.99f).ToString().Split('.')[0];
        timeCooldownArmor -= Time.fixedDeltaTime;

        if (timeCooldownArmor <= 0)
        {
            panelCooldownArmor.gameObject.SetActive(false);
            btnArmor.interactable = true;
            readyArmor = true;
        }
    }
    private void HealTimer()
    {
        if (readyHeal)
            return;

        textCooldownHeal.text = (timeCooldownHeal + 0.99f).ToString().Split('.')[0];
        timeCooldownHeal -= Time.fixedDeltaTime;

        if (timeCooldownHeal <= 0)
        {
            panelCooldownHeal.gameObject.SetActive(false);
            btnHeal.interactable = true;
            readyHeal = true;
        }
    }
    public void DisInteractableAllButtons()
    {
        button1.interactable = false;
        button2.interactable = false;
        button3.interactable = false;
        button4.interactable = false;
        button5.interactable = false;
        btnArmor.interactable = false;
        btnHeal.interactable = false;
    }
    public void DisableAllButton()
    {
        button1.gameObject.SetActive(false);
        button2.gameObject.SetActive(false);
        button3.gameObject.SetActive(false);
        button4.gameObject.SetActive(false);
        button5.gameObject.SetActive(false);
        btnArmor.gameObject.SetActive(false);
        btnHeal.gameObject.SetActive(false);
        UIManagerBars.Instance().DisableBars();
    }
    public void EnableAllButton()
    {
        button1.gameObject.SetActive(true);
        button2.gameObject.SetActive(true);
        button3.gameObject.SetActive(true);
        button4.gameObject.SetActive(true);
        button5.gameObject.SetActive(true);
        btnArmor.gameObject.SetActive(true);
        btnHeal.gameObject.SetActive(true);
        UIManagerBars.Instance().EnableBars();
    }
}
