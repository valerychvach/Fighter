﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    [Space(10)]
    [SerializeField] private Scrollbar sliderVolume;
    [SerializeField] private TextMeshProUGUI textSliderVolume;
    [Space(10)]
    [SerializeField] private Toggle toggleMute;
    [Space(10)]
    [SerializeField] private Transform settings;

    private float volume;
    private bool mute;

    public void ChangeVolume()
    {
        volume = sliderVolume.value * 100;
        textSliderVolume.text = volume.ToString();
        SoundManager.Instance().SetVolume(sliderVolume.value);
    }

    public void ChangeMute()
    {
        if (toggleMute.isOn)
        {
            mute = true;
        }
        else
        {
            mute = false;
        }
        SoundManager.Instance().SetMute(mute);
    }

    public void CloseSettingsMenu()
    {
        settings.gameObject.SetActive(false);
    }

}
