﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerMainMenu : MonoBehaviour
{
    [SerializeField] private Transform settings;

    public void Btn_StartGame()
    {
        SceneManager.LoadScene(1); // load scene for new game
    }
    public void Btn_Settings()
    {
        // Open submenu settings
        settings.gameObject.SetActive(true);
    }
    public void Btn_Exit()
    {
        Application.Quit(); // Exit from game
    }
}
