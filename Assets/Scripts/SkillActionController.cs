﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillActionController : MonoBehaviour
{


    // Instance
    private static SkillActionController instance;
    public static SkillActionController Instance()
    {
        return instance;
    }
    private void Start()
    {
        instance = this;
    }


    public void Button1()
    {
        if(UIManagerFight.Instance().readyButton1)
        {
            FightManager.Instance().Button1();
        }
    }
    public void Button2()
    {
        if (UIManagerFight.Instance().readyButton2)
        {
            FightManager.Instance().Button2();
        }
    }
    public void Button3()
    {
        if (UIManagerFight.Instance().readyButton3)
        {
            FightManager.Instance().Button3();
        }
    }
    public void Button4()
    {
        if (UIManagerFight.Instance().readyButton4)
        {
            FightManager.Instance().Button4();
        }
    }
    public void Button5()
    {
        if (UIManagerFight.Instance().readyButton5)
        {
            FightManager.Instance().Button5();
        }
    }
    public void Armor()
    {
        if (UIManagerFight.Instance().readyArmor)
        {
            FightManager.Instance().Armor();
        }
    }
    public void Heal()
    {
        if (UIManagerFight.Instance().readyHeal)
        {
            FightManager.Instance().Heal();
        }
    }
}
