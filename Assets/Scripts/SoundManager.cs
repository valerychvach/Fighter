﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioSource source;

    private List<AudioClip> listAudioClip;
    private List<string> listPath;

    private bool isPlay;

    private static SoundManager instance;
    public static SoundManager Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        GetDataFromStreamingAssets();
    }

    private void GetDataFromStreamingAssets()
    {
        PreparePathList();
        StartCoroutine(LoadMainMenuAudioCoroutine());
    }
    public void LoadAudioForLevel()
    {
        StartCoroutine(LoadLevelAudioCoroutine());
    }
    private void PreparePathList()
    {
        listPath = new List<string>();
        string pathToIntro = Application.dataPath + @"/StreamingAssets/DataMainMenu/intro.ogg";
        listPath.Add(pathToIntro);
    }
    private IEnumerator LoadMainMenuAudioCoroutine()
    {
        listAudioClip = new List<AudioClip>();
        WWW request = new WWW(listPath[0]);
        yield return request;
        listAudioClip.Add(request.GetAudioClip());

        PlayIntro(); // After loading - play intro
    }
    private IEnumerator LoadLevelAudioCoroutine()
    {
        string pathToLevelAudio = GameManager.Instance().CurrentPath + "Audio/";
        string pathToAudio1 = pathToLevelAudio + "sword1.ogg";
        listPath.Add(pathToAudio1);
        string pathToAudio2 = pathToLevelAudio + "sword2.ogg";
        listPath.Add(pathToAudio2);

        for (int i = 1; i < listPath.Count; i++)
        {
            WWW request = new WWW(listPath[i]);
            yield return request;
            listAudioClip.Add(request.GetAudioClip());
        }
    }

    public void PlayIntro()
    {
        source.clip = listAudioClip[0];
        source.Play();
    }
    public void StopIntro()
    {
        source.Stop();
    }

    public void PlaySword()
    {
        if (Random.Range(0, 100) <= 50)
        {
            source.clip = listAudioClip[1];
        }
        else
        {
            source.clip = listAudioClip[2];
        }

        source.Play();
    }
    public void SetVolume(float value)
    {
        source.volume = value;
    }
    public void SetMute(bool mute)
    {
        source.mute = mute;
    }
}
