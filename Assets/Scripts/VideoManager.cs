﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;

    [HideInInspector] public bool isPlay;
    [HideInInspector] public bool isPlayAfterGame;
    [HideInInspector] public bool isPlayAfterAfterGame;
    private bool isPlayStartVideo;
    

    // Instance
    private static VideoManager instance;
    public static VideoManager Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        StartCoroutine("CoroutineIdle");
        BeforeStartVideo();
        videoPlayer.Play();
        UIManagerFight.Instance().DisableAllButton();
        isPlayStartVideo = true;
        isPlayAfterGame = false;
        isPlayAfterAfterGame = false;
    }

    private void BeforeStartVideo()
    {
        //videoPlayer.url = GameManager.Instance().CurrentPath + "Video/beforestart.mp4";
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video2.mp4";
        videoPlayer.isLooping = false;
        isPlay = true;
    }
    public void Idle()
    {
        if (isPlay) return;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/idle.mp4";
        videoPlayer.isLooping = true;
        isPlay = false;
    }
    public void Button1()
    {
        if (isPlay) return;
        videoPlayer.isLooping = false;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video1.mp4";
        isPlay = true;
    }
    public void Button2()
    {
        if (isPlay) return;
        videoPlayer.isLooping = false;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video2.mp4";
        isPlay = true;
    }
    public void Button3()
    {
        if (isPlay) return;
        videoPlayer.isLooping = false;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video3.mp4";
        isPlay = true;
    }
    public void Button4()
    {
        if (isPlay) return;
        videoPlayer.isLooping = false;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video4.mp4";
        isPlay = true;
    }
    public void Button5()
    {
        if (isPlay) return;
        videoPlayer.isLooping = false;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video5.mp4";
        isPlay = true;
    }
    public void Armor()
    {
        if (isPlay) return;
        videoPlayer.isLooping = false;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video6.mp4";
        isPlay = true;
    }
    public void Heal()
    {
        if (isPlay) return;
        videoPlayer.isLooping = false;
        videoPlayer.url = GameManager.Instance().CurrentPath + "Video/video7.mp4";
        isPlay = true;
    }
    public void AfterGame()
    {
        videoPlayer.isLooping = false;
        videoPlayer.url = Application.dataPath + "/StreamingAssets/Video/videoaftergame.mp4";
        isPlay = true;
        isPlayAfterGame = false;
    }
    public void AfterGameHeroWinner()
    {
        videoPlayer.isLooping = false;
        videoPlayer.url = Application.dataPath + "/StreamingAssets/Video/videoaftergame.mp4";
        isPlay = true;
        isPlayAfterGame = false;
    }
    public void AfterGameOpponentWinner()
    {
        videoPlayer.isLooping = false;
        videoPlayer.url = Application.dataPath + "/StreamingAssets/Video/videoaftergame.mp4";
        isPlay = true;
        isPlayAfterGame = false;
    }
    private IEnumerator CoroutineIdle()
    {
        yield return new WaitForSeconds(1f);
        while (true)
        {
            if (!videoPlayer.isPlaying && videoPlayer.isPrepared)
            {
                if (isPlayAfterGame)
                {
                    AfterGame();
                    isPlayAfterAfterGame = true;
                    continue;
                }
                if (isPlayAfterAfterGame)
                {
                    UIManagerBars.Instance().GetWinner();
                    isPlayAfterAfterGame = false;
                    continue;
                }
                Idle();
                isPlay = false;
                if (isPlayStartVideo)
                {
                    UIManagerFight.Instance().EnableAllButton();
                    isPlayStartVideo = false;
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
