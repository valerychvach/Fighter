﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManagerBars : MonoBehaviour
{
    [Header("Enrage")]
    [SerializeField] private Transform enrage;
    [SerializeField] private TextMeshProUGUI textEnrage;
    [SerializeField] private TextMeshProUGUI textChangesEnrage;
    [Header("Super")]
    [SerializeField] private Transform super;
    [SerializeField] private TextMeshProUGUI textSuper;
    [SerializeField] private TextMeshProUGUI textChangesSuper;
    [Header("Health")]
    [SerializeField] private Transform health;
    [SerializeField] private TextMeshProUGUI textHealth;
    [SerializeField] private TextMeshProUGUI textChangesHealth;
    [Header("Armor")]
    [SerializeField] private Transform armor;
    [SerializeField] private TextMeshProUGUI textArmor;
    [SerializeField] private TextMeshProUGUI textChangesArmor;
    [Header("Game result")]
    [SerializeField] private TextMeshProUGUI textGameResult;
    [SerializeField] private Transform buttonOpenGameLog;
    [SerializeField] private Transform buttonCloseGameLog;
    [SerializeField] private Transform buttonRestartGame;
    [SerializeField] private Transform buttonGoToMainMenu;
    [SerializeField] private Transform logPlace;
    [SerializeField] private Transform logElementContainer;
    [SerializeField] private Transform logElementPrefab;
    [Header("Change names")]
    [SerializeField] private Transform inputField;
    [SerializeField] private TextMeshProUGUI newName;
    [SerializeField] private TextMeshProUGUI heroName;
    [SerializeField] private TextMeshProUGUI opponentName;
    [Header("Bar ransform")]
    [SerializeField] private Transform heroBar;
    [SerializeField] private Transform opponentBar;

    [HideInInspector] public float currentEnrage;
    [HideInInspector] public float maxEnrage;
    [HideInInspector] public float currentSuper;
    private float maxSuper;
    [HideInInspector] public float currentHealth;
    [HideInInspector] public float maxHealth;
    private float currentArmor;
    private float maxArmor;
    private bool isLogsOpened;
    private bool isMonsterNameChanged;
    private bool isWinner = false;
    private bool isWinnerHero;
    private bool isAlreadyWin;
    // Instance
    private static UIManagerBars instance;
    public static UIManagerBars Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        PrepareParameters();
    }
    private void FixedUpdate()
    {
        if (isWinner && !VideoManager.Instance().isPlay && !isAlreadyWin)
        {
            isAlreadyWin = true;
            VideoManager.Instance().isPlayAfterGame = true;
            UIManagerFight.Instance().DisableAllButton();
        }
    }
    public void GetWinner()
    {
        if (isWinnerHero)
        {
            HeroWin();
            UIManagerFight.Instance().DisableAllButton();
        }
        else
        {
            OpponentWin();
            UIManagerFight.Instance().DisableAllButton();
        }
    }
    private void PrepareParameters()
    {
        buttonOpenGameLog.gameObject.SetActive(false);
        buttonRestartGame.gameObject.SetActive(false);
        buttonGoToMainMenu.gameObject.SetActive(false);
        textGameResult.gameObject.SetActive(false);
        isLogsOpened = false;
        inputField.gameObject.SetActive(false);
        isWinner = false;
        isAlreadyWin = false;
        EnableBars();
    }
    private void HeroWin()
    {
        textGameResult.text = heroName.text + " win!";
        buttonOpenGameLog.gameObject.SetActive(true);
        buttonRestartGame.gameObject.SetActive(true);
        buttonGoToMainMenu.gameObject.SetActive(true);
        textGameResult.gameObject.SetActive(true);
    }
    private void OpponentWin()
    {
        textGameResult.text = opponentName.text + " win!";
        buttonOpenGameLog.gameObject.SetActive(true);
        buttonRestartGame.gameObject.SetActive(true);
        buttonGoToMainMenu.gameObject.SetActive(true);
        textGameResult.gameObject.SetActive(true);
    }

    // Enrage
    public void SetMaxEnrage(float _maxEnrage)
    {
        maxEnrage = _maxEnrage;
        currentEnrage = 0f;
        textEnrage.text = "Enrage: 0 / " + maxEnrage;
        enrage.localScale = new Vector3(0f, 1f, 1f);
    }
    public void AddEnrage(float _enrage)
    {
        currentEnrage += _enrage;
        if (currentEnrage < maxEnrage)
        {
            textEnrage.text = "Enrage: " + Mathf.FloorToInt(currentEnrage) + " / " + maxEnrage;
            float scaleX = currentEnrage / maxEnrage;
            enrage.localScale = new Vector3(scaleX, 1f, 1f);
        }
        else
        {
            currentEnrage = maxEnrage;
            textEnrage.text = "Enrage: " + maxEnrage + " / " + maxEnrage;
            enrage.localScale = new Vector3(1f, 1f, 1f);
            // monster win
            isWinner = true;
            isWinnerHero = true;
        }

        if (currentEnrage <= 0)
        {
            currentEnrage = 0f;
            textEnrage.text = "Enrage: " + currentEnrage + " / " + maxEnrage;
            enrage.localScale = new Vector3(0f, 1f, 1f);
        }

        // text changes value
        if (_enrage >= 0)
        {
            textChangesEnrage.text = "+" + Mathf.FloorToInt(_enrage);
            textChangesEnrage.color = Color.green;
        }
        else
        {
            textChangesEnrage.text = Mathf.FloorToInt(_enrage).ToString();
            textChangesEnrage.color = Color.red;
        }
    }
    // Super
    public void SetMaxSuper(float _maxSuper)
    {
        maxSuper = _maxSuper;
        currentSuper = 0f;
        textSuper.text = "Super: 0 / " + maxSuper;
        super.localScale = new Vector3(0f, 1f, 1f);
    }
    public void AddSuper(float _super)
    {
        if (_super < 0 && -_super > currentSuper)
            return;

        currentSuper += _super;
        if (currentSuper < maxSuper)
        {
            textSuper.text = "Super: " + Mathf.FloorToInt(currentSuper) + " / " + maxSuper;
            float scaleX = currentSuper / maxSuper;
            super.localScale = new Vector3(scaleX, 1f, 1f);
        }
        else
        {
            currentSuper = maxSuper;
            textSuper.text = "Super: " + maxSuper + " / " + maxSuper;
            super.localScale = new Vector3(1f, 1f, 1f);
        }

        // text changes value
        if (_super >= 0)
        {
            textChangesSuper.text = "+" + Mathf.FloorToInt(_super);
            textChangesSuper.color = Color.green;
        }
        else
        {
            textChangesSuper.text = Mathf.FloorToInt(_super).ToString();
            textChangesSuper.color = Color.red;
        }
        
    }
    // Health
    public void SetMaxHealth(float _maxHealth)
    {
        maxHealth = _maxHealth;
        currentHealth = _maxHealth;
        textHealth.text = "Health: " + Mathf.FloorToInt(currentHealth) + " / " + maxHealth;
        health.localScale = new Vector3(1f, 1f, 1f);
    }
    private void ReduseHealth(float _health)
    {
        currentHealth += _health;
        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
        if (currentHealth > 0)
        {
            textHealth.text = "Health: " + Mathf.FloorToInt(currentHealth) + " / " + maxHealth;
            float scaleX = currentHealth / maxHealth;
            health.localScale = new Vector3(scaleX, 1f, 1f);
        }
        else
        {
            currentHealth = 0f;
            textHealth.text = "Health: " + currentHealth + " / " + maxHealth;
            health.localScale = new Vector3(0, 1f, 1f);
            // fighter win
            isWinner = true;
            isWinnerHero = false;
        }

        // text changes value
        if (_health < 0)
        {
            textChangesHealth.text = Mathf.FloorToInt(_health).ToString();
            textChangesHealth.color = Color.red;
        }
        else
        {
            textChangesHealth.text = Mathf.FloorToInt(_health).ToString();
            textChangesHealth.color = Color.green;
        }
    }
    // Armor
    public void SetMaxArmor(float _maxArmor)
    {
        maxArmor = _maxArmor;
        currentArmor = _maxArmor;
        textArmor.text = "Armor: " + currentArmor + " / " + maxArmor;
        armor.localScale = new Vector3(1f, 1f, 1f);
    }
    public void ReduseArmor(float _armor)
    {
        currentArmor += _armor;
        if (currentArmor >= maxArmor)
        {
            currentArmor = maxArmor;
        }
        if (currentArmor > 0)
        {
            textArmor.text = "Armor: " + Mathf.FloorToInt(currentArmor) + " / " + maxArmor;
            float scaleX = currentArmor / maxArmor;
            armor.localScale = new Vector3(scaleX, 1f, 1f);
        }
        else
        {
            currentArmor = 0f;
            textArmor.text = "Armor: " + currentArmor + " / " + maxArmor;
            armor.localScale = new Vector3(0f, 1f, 1f);
        }

        // text changes value
        if (_armor < 0)
        {
            textChangesArmor.text = Mathf.FloorToInt(_armor).ToString();
            textChangesArmor.color = Color.red;
        }
        else
        {
            textChangesArmor.text = Mathf.FloorToInt(_armor).ToString();
            textChangesArmor.color = Color.green;
        }
    }
    // Get damage
    public void SetDamage(float _damage)
    {
        float percent = Random.Range(0.1f, 0.3f);
        float damage = _damage * (1 - percent);
        float armor = _damage * percent;

        if (damage > currentArmor)
        {
            damage -= currentArmor;
            armor = currentArmor;
        }

        if (currentArmor <= 0f)
        {
            damage *= 2f;
            armor = 0f;
        }
        // Burs
        ReduseHealth(damage);
        ReduseArmor(armor);
        // LOG
        FightManager.Instance().logElements[FightManager.Instance().logElements.Count - 1].Health = Mathf.FloorToInt(damage);
        FightManager.Instance().logElements[FightManager.Instance().logElements.Count - 1].Armor = Mathf.FloorToInt(armor);
    }
    // game log
    public void OpenGameLog()
    {
        if (!isLogsOpened)
        {
            isLogsOpened = true;
            for (int i = 0; i < FightManager.Instance().logElements.Count; i++)
            {
                Transform log = Instantiate(logElementPrefab, logElementContainer);
                TextMeshProUGUI[] texts = log.GetComponentsInChildren<TextMeshProUGUI>();
                texts[0].text = "Skill: " + FightManager.Instance().logElements[i].SkillName;
                texts[1].text = "Enrage: " + FightManager.Instance().logElements[i].Enrage + "\n Super: " + FightManager.Instance().logElements[i].Super;
                texts[2].text = "Health: " + FightManager.Instance().logElements[i].Health + "\n Armor: " + FightManager.Instance().logElements[i].Armor;
            }
        }
        
        logPlace.gameObject.SetActive(true);
    }
    public void CloseGameLog()
    {
        logPlace.gameObject.SetActive(false);
    }
    public void Restart()
    {
        buttonOpenGameLog.gameObject.SetActive(false);
        buttonCloseGameLog.gameObject.SetActive(false);
        SceneManager.LoadScene(2);
    }
    public void GoToMainMenu()
    {
        Destroy(GameObject.Find("SoundManager"));
        Destroy(GameObject.Find("GameManager"));
        SceneManager.LoadScene(0);
    }
    public void ChangeMonsterName()
    {
        isMonsterNameChanged = true;
        inputField.gameObject.SetActive(true);
    }
    public void ChangeFighterName()
    {
        isMonsterNameChanged = false;
        inputField.gameObject.SetActive(true);
    }
    public void OnNameChanged()
    {
        if (isMonsterNameChanged)
        {
            heroName.text = newName.text;
        }
        else
        {
            opponentName.text = newName.text;
        }
        inputField.gameObject.SetActive(false);
    }
    public void EnableBars()
    {
        heroBar.gameObject.SetActive(true);
        opponentBar.gameObject.SetActive(true);
    }
    public void DisableBars()
    {
        heroBar.gameObject.SetActive(false);
        opponentBar.gameObject.SetActive(false);
    }
}
