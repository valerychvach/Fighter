﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TinyJSON;
using System.IO;
using UnityEngine.SceneManagement;

public class FightManager : MonoBehaviour
{
    [HideInInspector] public List<Skill> skillList;
    private Hero monster;
    private Fighter fighter;

    private int numButton1ExceptionDamage = 0;
    private int numButton1ExceptionCooldown = 0;

    private bool isChangedCooldown = false;
    private float gameTime;
    [HideInInspector] public List<LogElement> logElements;
    private int currentLog;

    // Instance
    private static FightManager instance;
    public static FightManager Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        PrepareStartParameters();
        LoadSkillsFromJson();
        PrepareHeroes();
        PrepareBars();
    }
    private void FixedUpdate()
    {
        gameTime += Time.fixedDeltaTime;
    }
    private void PrepareStartParameters()
    {
        gameTime = 0f;
        logElements = new List<LogElement>();
        currentLog = -1;
    }
    private void PrepareHeroes()
    {
        monster = GameManager.Instance().GetMonster();
        fighter = GameManager.Instance().GetFighter();
    }
    private void LoadSkillsFromJson()
    {
        string fileName = "SkillsData.json";
        string loadPath = Application.dataPath + @"/StreamingAssets/HeroesVsOpponetns/SkillsData/" + fileName;
        string data = File.ReadAllText(loadPath);

        var dataFromJson = new TinyJsonSerializer().Deserialize<List<Skill>>(data);
        skillList = dataFromJson;
        UIButtons.Instance().SetButtonName();
    }
    private void PrepareSkills()
    {
        skillList = new List<Skill>();
        // Attack1
        Skill attack1 = new Skill();
        attack1.Name = "Attack1";
        attack1.EnrageFrom = 25f;
        attack1.EnrageTo = 35f;
        attack1.Super = 3f;
        attack1.DamageFrom = -50f;
        attack1.DamageTo = -35f;
        attack1.Cooldown = 0f;
        attack1.Armor = 0f;
        attack1.Ready = 0f;
        skillList.Add(attack1);
        // Attack2
        Skill attack2 = new Skill();
        attack2.Name = "Attack2";
        attack2.EnrageFrom = 50f;
        attack2.EnrageTo = 75f;
        attack2.Super = 7f;
        attack2.DamageFrom = -75f;
        attack2.DamageTo = -50f;
        attack2.Cooldown = 5f;
        attack2.Armor = 0f;
        attack2.Ready = 10f;
        skillList.Add(attack2);
        // Attack3
        Skill attack3 = new Skill();
        attack3.Name = "Attack3";
        attack3.EnrageFrom = 80f;
        attack3.EnrageTo = 100f;
        attack3.Super = 25f;
        attack3.DamageFrom = -150f;
        attack3.DamageTo = -120f;
        attack3.Cooldown = 15f;
        attack3.Armor = 0f;
        attack3.Ready = 30f;
        skillList.Add(attack3);
        // Button4
        Skill button4 = new Skill();
        button4.Name = "Button4";
        button4.EnrageFrom = 150f;
        button4.EnrageTo = 200f;
        button4.Super = -50f;
        button4.DamageFrom = -300f;
        button4.DamageTo = -200f;
        button4.Cooldown = 60f;
        button4.Armor = 0f;
        button4.Ready = 50f;
        skillList.Add(button4);
        // Combo1
        Skill combo1 = new Skill();
        combo1.Name = "Combo1";
        combo1.EnrageFrom = 250f;
        combo1.EnrageTo = 350f;
        combo1.Super = -80f;
        combo1.DamageFrom = -500f;
        combo1.DamageTo = -400f;
        combo1.Cooldown = 60f;
        combo1.Armor = 0f;
        combo1.Ready = 100f;
        skillList.Add(combo1);
        // Armor
        Skill armor = new Skill();
        armor.Name = "Armor";
        armor.EnrageFrom = -75f;
        armor.EnrageTo = -50f;
        armor.Super = 0f;
        armor.DamageFrom = 450f;
        armor.DamageTo = 550f;
        armor.Cooldown = 60f;
        armor.Armor = 0f;
        armor.Ready = 3f;
        skillList.Add(armor);
        // Heal
        Skill heal = new Skill();
        heal.Name = "Heal";
        heal.EnrageFrom = -120f;
        heal.EnrageTo = -80f;
        heal.Super = -25f;
        heal.DamageFrom = 250f;
        heal.DamageTo = 300f;
        heal.Cooldown = 60f;
        heal.Armor = 0f;
        heal.Ready = 50f;
        skillList.Add(heal);

        // save to json
        var data = new TinyJsonSerializer().Serialize(skillList);
        string fileName = "SkillsData.json";
        string savePath = Application.dataPath + @"/StreamingAssets/HeroesVsOpponetns/SkillsData/" + fileName;

        File.WriteAllText(savePath, data);
    }
    private void PrepareBars()
    {
        UIManagerBars.Instance().SetMaxEnrage(monster.Enrage);
        UIManagerBars.Instance().SetMaxSuper(monster.Super);
        UIManagerBars.Instance().SetMaxHealth(fighter.Health);
        UIManagerBars.Instance().SetMaxArmor(fighter.Armor);
    }

    #region Button actions
    private void UpdateBars(int numSkill)
    {
        // Prepare LogElement
        LogElement log = new LogElement();
        log.SkillName = skillList[numSkill].Name;
        logElements.Add(log);
        // Bars
        SetEnrage(numSkill);
        SetSuper(numSkill);
        SetDamage(numSkill);
        SetArmor(numSkill);
        // Sound
        SoundManager.Instance().PlaySword();
    }
    private void SetEnrage(int numSkill)
    {
        float enrageFrom = skillList[numSkill].EnrageFrom;
        float enrageTo = skillList[numSkill].EnrageTo;
        float enrage = Random.Range(enrageFrom, enrageTo);
        // Attack 3 will increase ENRAGE %30 more if Health is lower then %50
        if (numSkill == 2)
        {
            float healthPercent = UIManagerBars.Instance().currentHealth / UIManagerBars.Instance().maxHealth;
            if (healthPercent <= 0.5f)
            {
                enrage *= 0.3f;
            }
        }

        UIManagerBars.Instance().AddEnrage(enrage);
        // LOG
        logElements[logElements.Count - 1].Enrage = Mathf.FloorToInt(enrage);
    }
    private void SetSuper(int numSkill)
    {
        float super = skillList[numSkill].Super;
        UIManagerBars.Instance().AddSuper(super);
        // LOG
        logElements[logElements.Count - 1].Super = Mathf.FloorToInt(super);
    }
    private void SetDamage(int numSkill)
    {
        float damageFrom = skillList[numSkill].DamageFrom;
        float damageTo = skillList[numSkill].DamageTo;
        float damage = Random.Range(damageFrom, damageTo);
        // If user plays 3 time video 1, video 2 do not effects on warriors health
        if (numSkill == 1 && numButton1ExceptionDamage >= 3)
        {
            damage = 0f;
            numButton1ExceptionDamage = 0;
        }
        else if (numSkill == 2 && numButton1ExceptionDamage >= 3)
        {
            damage /= 2f;
        }

        // If button 4 pressed fr 30 second all buttons health reduction decrease 50 percent
        if (numSkill == 3 && gameTime <= 30f)
        {
            damage /= 2f;
        }

        UIManagerBars.Instance().SetDamage(damage);
    }
    private void SetArmor(int numSkill)
    {
        if (numSkill == 5)
        {
            UIManagerBars.Instance().ReduseArmor(skillList[numSkill].Armor);
        }
    }
    public void Button1()
    {
        numButton1ExceptionDamage++;
        numButton1ExceptionCooldown++;
        UIManagerFight.Instance().Button1Cooldown(skillList[0].Cooldown);
        UpdateBars(0);
        // Every attack 1 makes attack 2’s cooldown 2 sec less
        skillList[1].Cooldown -= numButton1ExceptionCooldown * 2;
        if (skillList[1].Cooldown <= 0) skillList[1].Cooldown = 0;

        VideoManager.Instance().Button1();
    }
    public void Button2()
    {
        UIManagerFight.Instance().Button2Cooldown(skillList[1].Cooldown);
        UpdateBars(1);
        // Exception: Attack 2’s cooldown gets half when ENRAGE is more then %50
        if (!isChangedCooldown && UIManagerBars.Instance().currentEnrage >= UIManagerBars.Instance().maxEnrage / 2)
        {
            skillList[2].Cooldown /= 2;
            isChangedCooldown = true;
        }

        VideoManager.Instance().Button2();
    }
    public void Button3()
    {
        UIManagerFight.Instance().Button3Cooldown(skillList[2].Cooldown);
        UpdateBars(2);

        VideoManager.Instance().Button3();
    }
    public void Button4()
    {
        UIManagerFight.Instance().Button4Cooldown(skillList[3].Cooldown);
        UpdateBars(3);

        VideoManager.Instance().Button4();
    }
    public void Button5()
    {
        UpdateBars(4);
        UIManagerFight.Instance().Button5Cooldown(skillList[4].Cooldown);
        VideoManager.Instance().Button5();
    }
    public void Armor()
    {
        UpdateBars(5);
        UIManagerFight.Instance().ArmorCooldown(skillList[5].Cooldown);
        VideoManager.Instance().Armor();
    }
    public void Heal()
    {
        UpdateBars(6);
        UIManagerFight.Instance().HealCooldown(skillList[6].Cooldown);
        VideoManager.Instance().Heal();
    }
    #endregion
}
