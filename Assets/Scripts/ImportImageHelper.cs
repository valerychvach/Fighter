﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImportImageHelper : MonoBehaviour
{
    private List<Image> listImageComponents;
    private List<string> listPathToImage;
    private Transform panel;
    private List<Sprite> listSprite;

    // Instance
    private static ImportImageHelper instance;
    public static ImportImageHelper Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    public void ImportImage(List<Image> _listImageComponents, List<string> _listPathToImage, Transform _panel)
    {
        listImageComponents = _listImageComponents;
        panel = _panel;
        listPathToImage = _listPathToImage;

        LoadSpritesForScene();
    }

    private void LoadSpritesForScene()
    {
        panel.gameObject.SetActive(false);
        StartCoroutine(LoadButtonsImage());
    }

    private IEnumerator LoadButtonsImage()
    {
        listSprite = new List<Sprite>();

        for (int i = 0; i < listPathToImage.Count; i++)
        {
            WWW request = new WWW(listPathToImage[i]);
            yield return request;
            Sprite sprite = Sprite.Create(request.texture, new Rect(0, 0, request.texture.width, request.texture.height), new Vector2(0.5f, 0.5f));
            listSprite.Add(sprite);
        }

        SetButtonSprite();
    }
    private void SetButtonSprite()
    {
        for (int i = 0; i < listImageComponents.Count; i++)
        {
            listImageComponents[i].sprite = listSprite[i];
        }

        panel.gameObject.SetActive(true);
    }
}
