﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeroManager : MonoBehaviour
{
    [SerializeField] private Transform btnPlay;
    [SerializeField] private Transform textSelectHero;

    private List<Hero> heroesList = new List<Hero>();
    private Fighter opponent;

    private void Start()
    {
        PrepareHeroes();
    }

    private void PrepareHeroes()
    {
        string fileNameHeroData = "HeroData";
        string pathHeroData = GameManager.Instance().CurrentPath + "Data/" + fileNameHeroData + ".txt";
        string loadDataHero = File.ReadAllText(pathHeroData);
        string[] parsedDataHero = loadDataHero.Split(' ');

        // Create monster (player)
        Hero hero = new Hero();
        hero.Enrage = float.Parse(parsedDataHero[0]);
        hero.Super = float.Parse(parsedDataHero[1]);
        heroesList.Add(hero);

        string fileNameOpponentData = "OpponentData";
        string pathOpponentData = GameManager.Instance().CurrentPath + "Data/" + fileNameOpponentData + ".txt";
        string loadDataOpponent = File.ReadAllText(pathOpponentData);
        string[] parsedDataOpponent = loadDataOpponent.Split(' ');

        // Create fighter
        opponent = new Fighter();
        opponent.Health = float.Parse(parsedDataOpponent[0]);
        opponent.Armor = float.Parse(parsedDataOpponent[1]);
    }

    private void SelectHero(int heroNum)
    {
        btnPlay.gameObject.SetActive(true);
        GameManager.Instance().SetHero(heroesList[0]);
        GameManager.Instance().SetOpponent(opponent);
    }

    #region ButtonAction
    public void SelectHero1()
    {
        SelectHero(0);
    }
    public void SelectHero2()
    {
        SelectHero(1);
    }
    public void SelectHero3()
    {
        SelectHero(2);
    }
    public void Play()
    {
        textSelectHero.gameObject.SetActive(false);
        SoundManager.Instance().StopIntro();
        SceneManager.LoadScene(2);
    }
    #endregion
}
