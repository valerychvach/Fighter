﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIButtons : MonoBehaviour
{
    [SerializeField] private List<Image> listImageButtonsComponents;
    [SerializeField] private Transform panelButtons;
    [SerializeField] private List<TextMeshProUGUI> listButtonName;

    private List<string> listPathToButtonImage;

    // Instance
    private static UIButtons instance;
    public static UIButtons Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        PreparePathToImage();
        ImportImageHelper.Instance().ImportImage(listImageButtonsComponents, listPathToButtonImage, panelButtons);
    }

    public void SetButtonName()
    {
        for (int i = 0; i < panelButtons.childCount; i++)
        {
            listButtonName[i].text = FightManager.Instance().skillList[i].Name;
        }
    }
    private void PreparePathToImage()
    {
        listPathToButtonImage = new List<string>();
        string pathToFolder = GameManager.Instance().CurrentPath + "Image/";

        string pathToButton1 = pathToFolder + "1.png";
        listPathToButtonImage.Add(pathToButton1);

        string pathToButton2 = pathToFolder + "2.png";
        listPathToButtonImage.Add(pathToButton2);

        string pathToButton3 = pathToFolder + "3.png";
        listPathToButtonImage.Add(pathToButton3);

        string pathToButton4 = pathToFolder + "4.png";
        listPathToButtonImage.Add(pathToButton4);

        string pathToButton5 = pathToFolder + "5.png";
        listPathToButtonImage.Add(pathToButton5);

        string pathToButton6 = pathToFolder + "Heal.png";
        listPathToButtonImage.Add(pathToButton6);

        string pathToButton7 = pathToFolder + "Armor.png";
        listPathToButtonImage.Add(pathToButton7);
    }
}
