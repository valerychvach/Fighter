﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerHeroes : MonoBehaviour
{
    [SerializeField] private List<Image> listImageHeroesComponents;
    [SerializeField] private Transform panelHeroes;

    private List<string> listPathToHeroesImage;

    // Instance
    private static UIManagerHeroes instance;
    public static UIManagerHeroes Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        LoadSpritesForScene();
    }
    private void LoadSpritesForScene()
    {
        PreparePathToImage();
        ImportImageHelper.Instance().ImportImage(listImageHeroesComponents, listPathToHeroesImage, panelHeroes);
    }
    private void PreparePathToImage()
    {
        listPathToHeroesImage = new List<string>();
        string pathToFolder = Application.dataPath + "/StreamingAssets/DataMainMenu/HeroesImage/";

        string pathToHero1 = pathToFolder + "Hero_str.jpg";
        listPathToHeroesImage.Add(pathToHero1);

        string pathToHero2 = pathToFolder + "Hero_abi.jpg";
        listPathToHeroesImage.Add(pathToHero2);

        string pathToHero3 = pathToFolder + "Hero_mag.jpg";
        listPathToHeroesImage.Add(pathToHero3);
    }
}
