﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill
{
    public string Name;
    public float EnrageFrom;
    public float EnrageTo;
    public float Super;
    public float DamageFrom;
    public float DamageTo;
    public float Cooldown;
    public float Armor;
    public float Ready;
}
