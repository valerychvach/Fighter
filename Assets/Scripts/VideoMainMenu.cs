﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoMainMenu : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;

    private void Start()
    {
        videoPlayer.url = Application.dataPath + "/StreamingAssets/DataMainMenu/videomainmenu.mp4";
        videoPlayer.isLooping = true;
        videoPlayer.Play();
    }
}
