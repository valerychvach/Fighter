﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [HideInInspector] public string CurrentPath;

    private string pathHeroVsOpponent;
    private string pathToSelectedLevel;
    private Hero hero;
    private Fighter opponent;
    
    // Instance
    private static GameManager instance;
    public static GameManager Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    private void Start()
    {
        PrepareData();
    }

    private void PrepareData()
    {
        pathHeroVsOpponent = Application.dataPath + @"/StreamingAssets/HeroesVsOpponetns/";
        pathToSelectedLevel = "Hero1VsOpponent1/";
        CurrentPath = pathHeroVsOpponent + pathToSelectedLevel;

        // Load data
        SoundManager.Instance().LoadAudioForLevel();
    }
    public void SetHero(Hero hero)
    {
        this.hero = hero;
    }
    public Hero GetMonster()
    {
        return hero;
    }
    public void SetOpponent(Fighter opponent)
    {
        this.opponent = opponent;
    }
    public Fighter GetFighter()
    {
        return opponent;
    }
}
